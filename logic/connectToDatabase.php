<?php

require_once(__DIR__ . '/../keys.php');

$host = getDatabaseHost();
$dbName = getDatabaseName();
$user = getDatabaseUser();
$password = getDatabasePassword();
$conn = new PDO ("mysql:host=$host;dbname=$dbName;charset=utf8", $user, $password);

$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Will be added to gitignore
