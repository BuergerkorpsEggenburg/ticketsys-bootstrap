<?php

//Load composer's autoloader
require_once __DIR__ . '/../vendor/autoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(__DIR__ . '/../keys.php');

function sendActivationEmail($emailAdress, $name, $code)
{
  $mainpage = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'];
  $link = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'].
    '/logic/user/verifyAccount.php?inputEmail_a='.$emailAdress . '&inputCode='.$code;

  $body = '
    Hallo '.$name.',<br>
    Danke für deine Registrierung auf Ticketsys! Dir fehlt leider noch der letzte Schritt bis zur Fertigstellung deines Accounts.<br>
    Du kannst auf <a href="'.$mainpage.'">Ticketsys</a> auf den Button <strong>>Verifizieren<</strong> klicken und deine Email-Adresse
    und diesen Code <strong>'.$code.'</strong> eingeben.<br>
    
    <a href="'.$link.'">Oder klicke einfach auf mich</a> und der Account wird ganz automatisch freigeschalten.<br><br>
    
    <i>Dies ist eine automatisierte Nachricht. Bitte antworten Sie nicht.</i>
  ';

  sendEmailToUser($emailAdress, 'Ticketsys', $name, 'Ticketsys - Account verifizierung', $body, $body);
}


function sendEmailToUser($emailAddress, $senderName, $name, $subject, $body, $altBody)
{
  $mail = new PHPMailer(false); // Passing `true` enables exceptions
  try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output

    $mail->CharSet = 'UTF-8';
    // Set mailer to use SMTP
    $mail->isSMTP();
    // Specify main and backup SMTP servers
    $mail->Host = getEmailHost();
    // Enable SMTP authentication
    $mail->SMTPAuth = true;
    // SMTP username
    $mail->Username = getEmailUsername();
    // SMTP password
    $mail->Password = getEmailPassword();
    // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPSecure = 'tls';
    // TCP port to connect to
    $mail->Port = getEmailPort();

    $mail->setFrom(getEmailUsername(), $senderName);

    // Send to
    $mail->addAddress($emailAddress, $name);

    $mail->addReplyTo(getEmailUsername(), 'Contact');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true); // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->AltBody = $altBody;

    $mail->send();
    //echo 'Message has been sent';
  } catch (Exception $e) {
    //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
  }
}
