<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 04.08.17
 * Time: 15:44
 */

if (!isset($_SESSION)) {
  session_start();
}

if (isset($_SESSION['email'])) {
  header('Location: /index.php?alertReason=login_alreadyLoggedIn');
  die();
}

if (!isset($_REQUEST['inputEmail_login']) OR empty($_REQUEST['inputEmail_login'])) {
  header('Location: /index.php?alertReason=login_isset_email');
  die();
}

if (!isset($_REQUEST['inputPassword_login']) OR empty($_REQUEST['inputPassword_login'])) {
  header('Location: /index.php?alertReason=login_isset_password');
  die();
}

$email = $_REQUEST['inputEmail_login'];
$inputPassword = $_REQUEST['inputPassword_login'];

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT UUID, confirmed FROM users WHERE email = :email;');
$stmt->bindParam(':email', $email);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $UUID = $row[0];
  $confirmed = $row[1];
  break;
}

if(isset($confirmed)) {
  if ($confirmed == 0) {
    header('Location: /index.php?alertReason=login_not_confirmed');
    die();
  }
}

$hashed_password = hash('sha512', $inputPassword . $UUID);

$login_was_successful = false;

$stmt = $conn->prepare('SELECT UUID, email, firstname, password, rank FROM users WHERE email = :email AND password = :password;');
$stmt->bindParam(':email', $email);
$stmt->bindParam(':password', $hashed_password);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $_SESSION['UUID'] = $row[0];
  $_SESSION['email'] = $row[1];
  $_SESSION['firstname'] = $row[2];
  $_SESSION['rank'] = $row[4];

  $login_was_successful = true;
  break;
}

if (!$login_was_successful) {
  header('Location: /index.php?alertReason=login_credentials_wrong');
} else {
  header('Location: /index.php?alertReason=login_successful');
}