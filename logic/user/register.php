<?php

if (!isset($_SESSION)) {
  session_start();
}

if (isset($_SESSION['email'])) {
  header('Location: /index.php?alert=alreadyLoggedIn');
  die();
}

//Load composer's autoloader
require_once __DIR__ . '/../../vendor/autoload.php';

use Respect\Validation\Validator as v;

$inputFirstname = $_REQUEST['inputFirstname'];
$inputSurname = $_REQUEST['inputSurname'];
$inputEmail = $_REQUEST['inputEmail'];
$inputPassword = $_REQUEST['inputPassword'];
$inputPassword_again = $_REQUEST['inputPassword_again'];

if (!isset($inputFirstname) OR empty($inputFirstname)) {
  header('Location: /index.php?alertReason=register_isset_firstname');
  die();
}
if (strlen($inputFirstname) > 128) {
  header('Location: /index.php?alertReason=register_firstname_over_128_characters');
  die();
}

if (!isset($inputSurname) OR empty($inputSurname)) {
  header('Location: /index.php?alertReason=register_isset_surname');
  die();
}
if (strlen($inputSurname) > 128) {
  header('Location: /index.php?alertReason=register_surname_over_128_characters');
  die();
}

if (!isset($inputEmail) OR empty($inputEmail)) {
  header('Location: /index.php?alertReason=register_isset_email');
  die();
}
if (strlen($inputEmail) > 128) {
  header('Location: /index.php?alertReason=register_email_over_128_characters');
  die();
}

if(!(v::email()->validate($inputEmail))) {
  header('Location: /index.php?alertReason=register_email_over_128_characters');
  die();
}

if (!isset($inputPassword) OR empty($inputPassword)) {
  header('Location: /index.php?alertReason=register_isset_password');
  die();
}
if (strlen($inputPassword) > 512) {
  header('Location: /index.php?alertReason=register_password_over_512_characters');
  die();
}

if (!isset($inputPassword_again) OR empty($inputPassword_again)) {
  header('Location: /index.php?alertReason=register_isset_password');
  die();
}

if ($inputPassword != $inputPassword_again) {
  header('Location: /index.php?alertReason=register_passwords_are_not_equal');
  die();
}

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT email FROM users WHERE email = :email;');
$stmt->bindParam(':email', $inputEmail);
$stmt->execute();
$isAlreadyUsed = $stmt->rowCount();

if ($isAlreadyUsed > 0) {
  header('Location: /index.php?alertReason=register_account_already_exist');
  die();
}

$code = rand(100000, 999999);

$stmt = $conn->prepare('INSERT INTO users(firstname, surname, email, rank, code, confirmed) 
                                VALUE (:firstname, :surname, :email, "user", :code, 0);');
$stmt->bindParam(':firstname', $inputFirstname);
$stmt->bindParam(':surname', $inputSurname);
$stmt->bindParam(':email', $inputEmail);
$stmt->bindParam(':code', $code);
$stmt->execute();

$stmt = $conn->prepare('SELECT UUID FROM users WHERE email = :email;');
$stmt->bindParam(':email', $inputEmail);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $UUID = $row[0];
  break;
}

if (isset($UUID)) {
  $hashed_password = hash('sha512', $inputPassword . $UUID);

  $stmt = $conn->prepare('UPDATE users SET password= :password WHERE UUID = :UUID;');
  $stmt->bindParam(':password', $hashed_password);
  $stmt->bindParam(':UUID', $UUID);
  $stmt->execute();
} else {
  header('Location: /index.php?alertReason=register_UUID_error');
  die();
}

require_once(__DIR__ . '/../emailHandler.php');
sendActivationEmail($inputEmail, $inputFirstname . ' ' . $inputSurname, $code);

header('Location: /index.php?alertReason=register_successful');
die();