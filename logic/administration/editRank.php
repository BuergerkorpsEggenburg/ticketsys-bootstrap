<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 06.08.17
 * Time: 18:00
 */

include('../ifNotLoggedInRedirectToIndex.php');
include('../ifNotEnoughPermissionRedirectToIndex.php');

if (!isset($_REQUEST['UUID']) OR empty($_REQUEST['UUID'])) {
  header('Location: /userManagement.php?alertReason=editRank_isset_UUID');
  die();
} else {
  if (!is_numeric($_REQUEST['UUID'])) {
    header('Location: /index.php?alertReason=editRank_isset_UUID');
    die();
  }
}
if (!isset($_REQUEST['inputeRank']) OR empty($_REQUEST['inputeRank'])) {
  header('Location: /userManagement.php?alertReason=editRank_isset_rank');
  die();
}

$UUID = $_REQUEST['UUID'];
$inputRank = $_REQUEST['inputeRank'];

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT firstname, surname FROM users WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $userName = $row[0] . ' ' . $row[1];
  break;
}

$stmt = $conn->prepare('UPDATE users SET rank = :rank WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->bindParam(':rank', $inputRank);
$stmt->execute();

header('Location: /userManagement.php?alertReason=editRank_successful&userName=' . $userName);
die();