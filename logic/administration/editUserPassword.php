<?php
/**
 * Created by PhpStorm.
 * User: dafnik
 * Date: 06.08.17
 * Time: 18:00
 */

include('../ifNotLoggedInRedirectToIndex.php');
include('../ifNotEnoughPermissionRedirectToIndex.php');

if (!isset($_REQUEST['UUID']) OR empty($_REQUEST['UUID'])) {
  header('Location: /userManagement.php?alertReason=editUserPassword_isset_UUID');
  die();
} else {
  if (!is_numeric($_REQUEST['UUID'])) {
    header('Location: /index.php?alertReason=editUserPassword_isset_UUID');
    die();
  }
}

if (!isset($_REQUEST['inputePassword']) OR empty($_REQUEST['inputePassword'])) {
  header('Location: /userManagement.php?alertReason=editUserPassword_isset_password');
  die();
}
if (!isset($_REQUEST['inputePassword_again']) OR empty($_REQUEST['inputePassword_again'])) {
  header('Location: /userManagement.php?alertReason=editUserPassword_isset_password_again');
  die();
}

$UUID = $_REQUEST['UUID'];
$inputPassword = $_REQUEST['inputePassword'];
$inputPassword_again = $_REQUEST['inputePassword_again'];

if ($inputPassword != $inputPassword_again) {
  header('Location: /userManagement.php?alertReason=editUserPassword_passwords_are_not_equal');
  die();
}

if (strlen($inputPassword) > 512) {
  header('Location: /userManagement.php?alertReason=editUserPassword_passwords_only_512_characters');
  die();
}

$hashed_password = hash('sha512', $inputPassword . $UUID);

if (!isset($conn)) {
  include "../connectToDatabase.php";
}

$stmt = $conn->prepare('SELECT firstname, surname FROM users WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->execute();

while ($row = $stmt->fetch()) {
  $userName = $row[0] . ' ' . $row[1];
  break;
}

$stmt = $conn->prepare('UPDATE users SET password = :password WHERE UUID = :UUID;');
$stmt->bindParam(':UUID', $UUID);
$stmt->bindParam(':password', $hashed_password);
$stmt->execute();

header('Location: /userManagement.php?alertReason=editUserPassword_successful&userName=' . $userName);
die();