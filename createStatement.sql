DROP DATABASE IF EXISTS ticketsys;
CREATE DATABASE ticketsys;
USE ticketsys;

CREATE TABLE IF NOT EXISTS users(
  UUID      INT          NOT NULL AUTO_INCREMENT,
  email     VARCHAR(128) NOT NULL,
  password  VARCHAR(512),

  firstname VARCHAR(128) NOT NULL,
  surname   VARCHAR(128) NOT NULL,

  rank		ENUM('administrator', 'moderator', 'user') NOT NULL,

  code      INT         NOT NULL,
  confirmed TINYINT     NOT NULL,

  PRIMARY KEY(UUID)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS movies(
  UMID                INT          NOT NULL AUTO_INCREMENT,
  name                VARCHAR(128) NOT NULL,
  date                DATE     NOT NULL,
  trailerLink         VARCHAR(256),
  workerUUID          INT          DEFAULT NULL,
  emergencyWorkerUUID INT          DEFAULT NULL,

  bookedCards         TINYINT DEFAULT 0,

  PRIMARY KEY(UMID),
  FOREIGN KEY (workerUUID) REFERENCES users(UUID) ON DELETE CASCADE,
  FOREIGN KEY (emergencyWorkerUUID) REFERENCES users(UUID) ON DELETE CASCADE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS bookings(
  UBID BIGINT NOT NULL AUTO_INCREMENT,
  UUID INT    NOT NULL,
  UMID INT    NOT NULL,

  count TINYINT NOT NULL,

  PRIMARY KEY(UBID),
  FOREIGN KEY (UUID) REFERENCES users(UUID) ON DELETE CASCADE,
  FOREIGN KEY (UMID) REFERENCES movies(UMID) ON DELETE CASCADE
) ENGINE=InnoDB;

INSERT INTO movies(name, date, trailerLink) VALUE ("Titanic", "2018-05-03", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Titanic 2", "2018-05-04", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Matrix", "2018-05-05", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Matrix Reloaded", "2018-05-06", "https://google.com");

INSERT INTO movies(name, date, trailerLink) VALUE ("Kevin allein zuhause", "2018-05-10", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Kevin allein zuhause 2", "2018-05-11", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Kevin allein zuhause 3", "2018-05-12", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Zurück in die Zukunft 1", "2018-05-13", "https://google.com");

INSERT INTO movies(name, date, trailerLink) VALUE ("Zurück in die Zukunft 2", "2018-05-17", "https://google.com");
INSERT INTO movies(name, date, trailerLink) VALUE ("Zurück in die Zukunft 3", "2018-05-18", "https://google.com");